﻿using System;
using EMovies.data;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace EMovies
{
    public partial class App : Application
    {
        public static MoviesManager MoviesManager { get; private set; }

        public App()
        {
            InitializeComponent();
            MoviesManager = new MoviesManager(new RestService());
            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            AppCenter.Start("android=fbbc81a3-13d5-4fca-88f5-893ab31fa0ae;" +
                  "uwp={Your UWP App secret here};" +
                  "ios=a033c620-b39c-476a-81d3-b374b0c4300f",
                  typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}

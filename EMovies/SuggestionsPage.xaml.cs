﻿using EMovies.data;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TMDbLib.Objects.Search;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace EMovies
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SuggestionsPage : ContentPage
	{
        private SearchMovie _movie;

        public SuggestionsPage(SearchMovie movie)
        {
            InitializeComponent();
            _movie = movie;
            BindingContext = movie;
            GetSimilarMovies();
        }

        async void GetSimilarMovies()
        {
            try
            {
                var client = new HttpClient();
                var tasteDiveApi = "https://tastedive.com/api/similar?k=335823-Emovies-5ME6DS05&q=";
                tasteDiveApi += System.Net.WebUtility.UrlEncode(_movie.Title) + "&type=movies&info=1";
                Uri uri;

                uri = new Uri(tasteDiveApi);
                SimilarMovies simMovies = new SimilarMovies();
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    simMovies = JsonConvert.DeserializeObject<SimilarMovies>(jsonContent);
                    if (simMovies.similar.results.Count > 0)
                    {
                        simMovies.similar.results.ForEach((movie) =>
                        {
                            movie.Overview = movie.Overview.Replace("\n", String.Empty);
                        });
                    }
                    else
                    {
                        SimilarMoviesListView.IsVisible = false;
                        NoSimMovies.IsVisible = true;
                    }
                }
                else
                {
                    SimilarMoviesListView.IsVisible = false;
                    NoSimMovies.IsVisible = true;
                    throw new Exception("No data");
                }
                SimilarMoviesListView.ItemsSource = new ObservableCollection<Info>(simMovies.similar.results);
            }
            catch (Exception e)
            {
                SimilarMoviesListView.IsVisible = false;
                NoSimMovies.IsVisible = true;
                Crashes.TrackError(e);
                await DisplayAlert("Error", e.Message, "Close");
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadMoviePoster();
        }

        private void LoadMoviePoster()
        {
            if (_movie.PosterPath != null) return;

            PosterMovie.BackgroundColor = Color.Gray;
        }

        async void HandleNavigateToSearchTab(object sender, EventArgs args)
        {
            if (!(sender is ViewCell item) || item.BindingContext == null)
            {
                return;
            }

            var movie = item.BindingContext as Info;
            MessagingCenter.Send<SuggestionsPage, string>(this, "Pop", movie.Title);
            Analytics.TrackEvent("InterestedBySimilarMovie", new Dictionary<string, string>
            {
                { "MovieTitle", movie.Title }
            });
            await Navigation.PopToRootAsync();
        }
    }
}
﻿using System.Collections.Generic;
using TMDbLib.Client;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.Search;

namespace EMovies.data
{
    public class RestService : IRestService
    {
        private TMDbClient _client;
        private string _lastSearch;
        private int _currentPageSearch = 0;
        private int _totalPages = 0;

        public int TotalResults
        {
            get;
            private set;
        }

        public int TotalResultsFetched
        {
            get;
            private set;
        }

        public RestService()
        {
            _client = new TMDbClient(Constants.TMDbLibAPIKey, true);
        }

        public List<SearchMovie> RefreshDataAsync(string search = null)
        {
            SearchContainer<SearchMovie> results = _client.SearchMovieAsync(search, 0, true).Result;
            TotalResults = results.TotalResults;
            TotalResultsFetched = results.Results.Count;
            _lastSearch = search;
            _currentPageSearch = results.Page;
            _totalPages = results.TotalPages;
            return results.Results;       
        }

        public List<SearchMovie> FetchMoreData(string search = null)
        {
            if (search != _lastSearch || _currentPageSearch >= _totalPages) return null;

            SearchContainer<SearchMovie> results = _client.SearchMovieAsync(search, _currentPageSearch + 1, true).Result;
            TotalResults = results.TotalResults;
            TotalResultsFetched += results.Results.Count;
            _currentPageSearch = results.Page;
            return results.Results;
        }

        public List<Video> GetMovieVideos(int id)
        {
            var results = _client.GetMovieVideosAsync(id).Result;
            var toto = _client.GetMovieCreditsAsync(id).Result;
            return results.Results;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMovies.data
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class SimilarMovies
    {
        [JsonProperty("Similar")]
        public Similar similar { get; set; }
    }

    public partial class Similar
    {
        [JsonProperty("Info")]
        public List<Info> info { get; set; }

        [JsonProperty("Results")]
        public List<Info> results { get; set; }
    }

    public partial class Info
    {
        [JsonProperty("Name")]
        public string Title { get; set; }

        [JsonProperty("Type")]
        public string type { get; set; }

        [JsonProperty("wTeaser")]
        public string Overview { get; set; }

        [JsonProperty("wUrl")]
        public Uri wUrl { get; set; }

        [JsonProperty("yUrl")]
        public Uri yUrl { get; set; }

        [JsonProperty("yID")]
        public string yId { get; set; }
    }
}

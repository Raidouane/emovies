﻿using System;
using System.Collections.Generic;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Search;

namespace EMovies.data
{
    public interface IRestService
    {
        int TotalResults
        {
            get;
        }

        int TotalResultsFetched
        {
            get;
        }

        List<SearchMovie> RefreshDataAsync(string search);
        List<Video> GetMovieVideos(int id);
        List<SearchMovie> FetchMoreData(string search);
    }
}

﻿using System.Collections.Generic;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Search;

namespace EMovies.data
{
    public class MoviesManager
    {
        IRestService _restService;

        public MoviesManager(IRestService service)
        {
            _restService = service;
        }

        public List<SearchMovie> RefreshDataAsync(string search)
        {
            return _restService.RefreshDataAsync(search);
        }

        public List<SearchMovie> FetchMoreData(string search)
        {
            return _restService.FetchMoreData(search);
        }

        public List<Video> GetMovieVideos(int id)
        {
            return _restService.GetMovieVideos(id);
        }

        public int GetTotalResults()
        {
            return _restService.TotalResults;
        }

        public int GetTotalResultsFetched()
        {
            return _restService.TotalResultsFetched;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Xamarin.Forms.Maps;
using Xamarin.Forms;
using System.Net.Http;
using System.Diagnostics;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace EMovies
{
    public partial class MapPage : ContentPage
    {
        Map map;
        public MapPage()
        {   
            map = new Map(
                MapSpan.FromCenterAndRadius(
                    new Position(33.133893, -117.169367), Distance.FromMiles(12)))
            {
                IsShowingUser = false,
                HeightRequest = 100,
                WidthRequest = 960,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
        }

        override protected void OnAppearing()
        {
            tryGeoLocation();
            Analytics.TrackEvent("Map", new Dictionary<string, string>
            {
                { "Map", "Go on Map" }
            });
        }

        private async void tryGeoLocation()
        {
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
                    {
                        await DisplayAlert("Need location", "Gunna need that location", "OK");
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
                    status = results[Permission.Location];
                }

                if (status == PermissionStatus.Granted)
                {
                    //Permission granted, do what you want do.
                    map.IsShowingUser = true;
                    addPins(map);
                }
                else
                {
                    map.IsShowingUser = false;
                    addPins(map);
                    await DisplayAlert("Location Denied", "Can not continue, try again.", "OK");
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                await DisplayAlert("Error", ex.Message, "OK");
            }
        }

        private void addPins(Map map)
        {
            var pin1 = new Pin
            {
                Position = new Position(33.134727, -117.192525),
                Type = PinType.Place,
                Label = "Edwards San Marcos Stadium 18",
            };
            var pin2 = new Pin
            {
                Position = new Position(33.135000, -117.124957),
                Type = PinType.Place,
                Label = "Edwards San Marcos Cinema",
            };
            var pin3 = new Pin
            {
                Position = new Position(33.105549, -117.244069),
                Type = PinType.Place,
                Label = "Cinepolis Luxury Cinemas - La Costa",
            };
            var pin4 = new Pin
            {
                Position = new Position(33.200279, -117.266984),
                Type = PinType.Place,
                Label = "Cinepolis Vista",
            };
            var pin5 = new Pin
            {
                Position = new Position(33.129185, -117.091262),
                Type = PinType.Place,
                Label = "Del Norte Plaza Movies 8",
            };
            var pin6 = new Pin
            {
                Position = new Position(33.121636, -117.086886),
                Type = PinType.Place,
                Label = "Regal Cinemas Escondido Stadium 16",
            };
            var pin7 = new Pin
            {
                Position = new Position(33.124410, -117.083793),
                Type = PinType.Place,
                Label = "Edwards Vineyard Twin",
            };
            var pin8 = new Pin
            {
                Position = new Position(33.050069, -117.262113),
                Type = PinType.Place,
                Label = "AMC Wiegand Plaza 8",
            };
            var pin9 = new Pin
            {
                Position = new Position(33.130454, -117.066975),
                Type = PinType.Place,
                Label = "Edwards Carousel Cinema 6",
            };
            var pin10 = new Pin
            {
                Position = new Position(33.177500, -117.326641),
                Type = PinType.Place,
                Label = "Moviemax Theatres at Westfield Plaza Camino Real",
            };
            var pin11 = new Pin
            {
                Position = new Position(33.178617, -117.329062),
                Type = PinType.Place,
                Label = "Regal Cinemas Carlsbad 12",
            };
            var pin12 = new Pin
            {
                Position = new Position(33.181429, -117.326898),
                Type = PinType.Place,
                Label = "Town & Country 4 Theatres",
            };

            map.Pins.Add(pin1);
            map.Pins.Add(pin2);
            map.Pins.Add(pin3);
            map.Pins.Add(pin4);
            map.Pins.Add(pin5);
            map.Pins.Add(pin6);
            map.Pins.Add(pin7);
            map.Pins.Add(pin8);
            map.Pins.Add(pin9);
            map.Pins.Add(pin10);
            map.Pins.Add(pin11);
            map.Pins.Add(pin12);

            var slider = new Slider(8, 15, 1);
            slider.ValueChanged += (sender, e) =>
            {
                var zoomLevel = e.NewValue; // between 1 and 18
                var latlongdegrees = 360 / (Math.Pow(2, zoomLevel));
                Debug.WriteLine(zoomLevel + " -> " + latlongdegrees);
                if (map.VisibleRegion != null)
                    map.MoveToRegion(new MapSpan(map.VisibleRegion.Center, latlongdegrees, latlongdegrees));
            };

            var stack = new StackLayout { Spacing = 0 };
            stack.Children.Add(map);
            stack.Children.Add(slider);
            Content = stack;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Microsoft.AppCenter.Analytics;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using TMDbLib.Objects.Search;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace EMovies
{
    public partial class MoviePage : ContentPage
    {
        private string _videoYoutubeKey = "";
        private SearchMovie _movie;

        public MoviePage(SearchMovie movie)
        {
            InitializeComponent();
            _movie = movie;
            BuildReleaseDate();
            BindingContext = movie;
            LoadYoutubePlayer();
        }

        private void BuildReleaseDate()
        {
            if (_movie.ReleaseDate == null)
            {
                MovieReleaseDate.Text = "N/A";
                return;
            } 
            DateTime date = (DateTime)_movie.ReleaseDate;
            MovieReleaseDate.Text = $"{date.Month}/{date.Day}/{date.Year}";
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadMoviePoster();
            var videos = App.MoviesManager.GetMovieVideos(_movie.Id);
            if (videos == null || videos.Count == 0) 
            {
                TrailerLoader.IsRunning = false;
                TrailerFetchText.Text = "No trailer for this movie...";
                return;
            }

            _videoYoutubeKey = videos[0].Key;
            DisplayYoutubePlayer();
        }

        private void LoadMoviePoster()
        {
            if (_movie.PosterPath != null) return;

            PosterMovie.BackgroundColor = Color.Gray;
        }

        private void LoadYoutubePlayer()
        {
            var width = GetWidthVideo();
            TrailerLoaderContainer.WidthRequest = width;
            TrailerLoaderContainer.HeightRequest = GetHeightVideoByWidthInLargeFormat(width);
        }

        private void DisplayYoutubePlayer()
        {
            UpdateTrailerWebViewDimensions();
            var css = "style='margin: 0px;padding: 0px;border: 0px;width: 100%;height: 100%;background-color: transparent;'";
            var htmlSource = new HtmlWebViewSource
            {
                Html = $@"<html {css}><body {css}>
            <iframe width=""100%"" height=""100%"" src=""https://www.youtube.com/embed/{_videoYoutubeKey}"" frameborder=""0"" allow=""accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"" allowfullscreen></iframe>
            </body></html>"
            };

            TrailerLoaderContainer.IsVisible = false;
            Trailer.Source = htmlSource;
        }

        private double GetHeightVideoByWidthInLargeFormat(double width)
        {
            double widthRef = 16;
            double heightRef = 9;
            return (width * heightRef) / widthRef;
        }

        private double GetWidthVideo()
        {
            var mainDisplayInfo = DeviceDisplay.MainDisplayInfo;
            return mainDisplayInfo.Width / mainDisplayInfo.Density;
        }

        private void UpdateTrailerWebViewDimensions()
        {
            var mainDisplayInfo = DeviceDisplay.MainDisplayInfo;
            var width = GetWidthVideo();
            Trailer.WidthRequest = width;
            Trailer.HeightRequest = GetHeightVideoByWidthInLargeFormat(width);
        }

        private async void HandleOpenMoviePoster(object sender, EventArgs args)
        {
            if (_movie.PosterPath == null) return;

            Analytics.TrackEvent("OpenPoster", new Dictionary<string, string>
            {
                { "MovieTitle", _movie.Title }
            });
            await PopupNavigation.Instance.PushAsync(new CoverMoviePopUp(_movie.PosterPath));
        }

        async void HandleNavigateToSuggestionsPage(object sender, EventArgs args)
        {
            Analytics.TrackEvent("GoToSimilarMoviesPage", new Dictionary<string, string>
            {
                { "MovieTitle", _movie.Title }
            });
            await Navigation.PushAsync(new SuggestionsPage(_movie));
        }
    }
}

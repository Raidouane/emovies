﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using TMDbLib.Objects.Search;
using Xamarin.Forms;

namespace EMovies
{
    class SearchBarBehavior : Behavior<SearchBar>
    {
        protected override void OnAttachedTo(SearchBar bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.TextChanged += HandleTextChanged;
        }

        protected override void OnDetachingFrom(SearchBar bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.TextChanged -= HandleTextChanged;
        }

        void HandleTextChanged(object sender, TextChangedEventArgs e)
        {
            bool IsValid = e.NewTextValue.Length > 0 ? true : false;           
            ((SearchBar)sender).BackgroundColor = IsValid ? Color.FromHex("#ffffff") : Color.FromHex("#f3a9a9");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EMovies
{
    public static class Utils
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable) => !(enumerable?.Any() ?? false);
    }
}

﻿using EMovies.data;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using TMDbLib.Objects.Search;
using Xamarin.Forms;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using System.Collections.Generic;
using System.Linq;

namespace EMovies
{
    public partial class SearchTab : ContentPage
    {
        public string Results { get; private set; } = null;
        public bool IsLoading { get; private set; }
        private ObservableCollection<SearchMovie> _movies;
        private string _currentSearch;

        public SearchTab()
        {
            InitializeComponent();
            ResizeElements();
            BindingContext = this;

            MessagingCenter.Subscribe<SuggestionsPage, string>(this, "Pop", (sender, arg) => {
                _currentSearch = arg;
                Search.Text = arg;
                HandleValidateSearch(null, null);
            });
        }

        private void ResizeElements()
        {
            if (Xamarin.Forms.Device.RuntimePlatform != Xamarin.Forms.Device.Android)
            {
                return;
            }

            SearchContainer.Padding = new Thickness(10);
            Search.BackgroundColor = Color.White;
        }

        private void LoadItems()
        {
            IsLoading = true;
            var movies = App.MoviesManager.FetchMoreData(_currentSearch);
            if (movies == null)
            {
                IsLoading = false;
                return;
            }

            foreach (var movie in movies)
            {
                _movies.Add(movie);
            }
            UpdateResultsNumber();
            IsLoading = false;
        }

        private int GetTotalResults()
        {
            return App.MoviesManager.GetTotalResults();
        }

        private int GetTotalResultsFetched()
        {
            return App.MoviesManager.GetTotalResultsFetched();
        }

        private void UpdateResultsNumber()
        {
            Results = string.Format("{0}/{1}", GetTotalResultsFetched(), GetTotalResults());
            OnPropertyChanged(nameof(Results));
        }

        private async Task FetchByMovieSearchAsync(string search)
        {
            try
            {
                _currentSearch = search;
                var moviesFromApi = App.MoviesManager.RefreshDataAsync(_currentSearch);
                if (moviesFromApi.IsNullOrEmpty())
                {
                    throw new Exception("No movies found.");
                }

                _movies = new ObservableCollection<SearchMovie>(moviesFromApi);
                UpdateResultsNumber();
                SearchHeader.IsVisible = true;
                SearchMovieListView.ItemsSource = _movies;
                SearchMovieListView.ItemAppearing += async (sender, e) =>
                {
                    if (IsLoading || _movies.Count == 0)
                        return;

                    bool isHittingBottom = e.Item == _movies[_movies.Count - 1];
                    if (isHittingBottom)
                    {
                       LoadItems();
                    }
                };
            }
            catch (Exception e)
            {
                SearchMovieListView.ItemsSource = new ObservableCollection<SearchMovie>();
                SearchHeader.IsVisible = false;
                Crashes.TrackError(e);
                await DisplayAlert("Error", e.Message, "Close");
            }
        }

        async void HandleNavigateToMoviePage(object sender, EventArgs args)
        {
            if (!(sender is ViewCell item) || item.BindingContext == null)
            {
                return;
            }

            var movie = item.BindingContext as SearchMovie;
            Analytics.TrackEvent("GoToMoviePage", new Dictionary<string, string>
            {
                { "MovieTitle", Search.Text }
            });
            await Navigation.PushAsync(new MoviePage(movie));
        }

        async void HandleValidateSearch(object sender, EventArgs args)
        {
            if (string.IsNullOrEmpty(Search.Text)) return;

            IsLoading = true;
            Analytics.TrackEvent("SearchMovies", new Dictionary<string, string>
            {
                { "MovieTitle", Search.Text }
            });
            await FetchByMovieSearchAsync(Search.Text);
            IsLoading = false;
        }
    }
}
